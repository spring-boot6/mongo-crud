package com.mongo.service;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class ProcessIncomingDataModelImplTest {
	
	@Autowired
	ProcessIncomingDataModelImpl processIncomingDataModelImpl;

	
	@Before
	public void setup() {
		
	}
	
	@Test
	public void findLargest() {
		Integer[] arr=new Integer[]{ 91,42,3,4,25,61,7,38,9,10 };
		Integer result=processIncomingDataModelImpl.findLargest(Arrays.asList(arr));
		Assert.assertNotNull(result);
		Assert.assertEquals(91L, result.longValue());
	}
	
	
	@Test
	public void RemoveWhiteSpaces() {
		String data ="This is test data";
		String expected ="Thisistestdata";
		String result=processIncomingDataModelImpl.RemoveWhiteSpaces(data);
		Assert.assertNotNull(result);
		Assert.assertEquals(expected, result);
	}
	
	

}
