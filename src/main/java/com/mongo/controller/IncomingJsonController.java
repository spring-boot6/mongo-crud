package com.mongo.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.exc.MismatchedInputException;
import com.mongo.Exception.ArgumentsNotValidException;
import com.mongo.Exception.ErrorResponse;
import com.mongo.delegate.IncomingDataModelDelegate;
import com.mongo.model.IncomingDataModel;
import com.mongo.model.validation.IncomingDataValidator;

import lombok.extern.log4j.Log4j2;

@Log4j2
@RestController
@RequestMapping("api/")
public class IncomingJsonController {
	
	@Autowired
	MessageSource messageResource;
	
	@Autowired
	IncomingDataValidator validator;

	@Autowired
	IncomingDataModelDelegate incomingDataModelDelegate;
	
	@PostMapping("loadJson")
	IncomingDataModel incomingJsonData(@RequestBody IncomingDataModel  incomingDataModel, BindingResult bindingResult) throws Exception, RuntimeException   {
		log.debug("\n\n jsonData recieved:"+incomingDataModel);
		validator.validate(incomingDataModel, bindingResult);
		 if (bindingResult.hasErrors()) {
			 throw new  ArgumentsNotValidException(bindingResult);
		 }
		 IncomingDataModel response= incomingDataModelDelegate.save(incomingDataModel);
		 return response;
	}
	

	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(ArgumentsNotValidException.class)
	public ErrorResponse handleValidationExceptions(ArgumentsNotValidException ex) {
		Map<String, String> errors = new HashMap<>();
		ex.getBindingResult().getAllErrors().forEach((error) -> { 
			String fieldName = ((FieldError) error).getField();
			String errorMessage = error.getDefaultMessage(); 
			errors.put(fieldName,messageResource.getMessage(errorMessage, null, LocaleContextHolder.getLocale()));
			});
		
		ErrorResponse response=new ErrorResponse();
		response.setErrorCode(HttpStatus.BAD_REQUEST.toString());
		response.setMessage(HttpStatus.BAD_REQUEST.name());
		response.setErrorDetails(errors);
		return response;
	}
	//=================================
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(HttpMessageNotReadableException.class)
	public ErrorResponse handleProperDataTypeExceptions(HttpMessageNotReadableException ex) {
		ErrorResponse response=new ErrorResponse();
		Map<String, String> errors = new HashMap<>();
		
		MismatchedInputException cause=null;
		if(ex.getCause()!=null&& ex.getCause() instanceof MismatchedInputException ) {
			cause=(MismatchedInputException)ex.getCause();
			response.setMessage(cause.getMessage());
			errors.put(cause.getPathReference(), cause.getOriginalMessage());
		}else {
			response.setMessage(ex.getMessage());
		}
		response.setErrorDetails(errors);
		response.setErrorCode(HttpStatus.BAD_REQUEST.toString());
		return response;
	}	
	
	
	

}
