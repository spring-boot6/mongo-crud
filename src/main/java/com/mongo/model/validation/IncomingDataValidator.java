package com.mongo.model.validation;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.mongo.Exception.ExceptionCodes;
import com.mongo.model.IncomingDataModel;

@Component
public class IncomingDataValidator implements Validator{


    public boolean supports(Class<?> aClass) {
        return IncomingDataModel.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
    	IncomingDataModel data = (IncomingDataModel) o;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "findDuplicates", ExceptionCodes.FILED_NOT_EMPTY, "field.NotEmpty");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "whiteSpacesGalore", ExceptionCodes.FILED_NOT_EMPTY, "field.NotEmpty");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "validateMeOnlyIActuallyShouldBeABoolean", ExceptionCodes.FILED_NOT_EMPTY, "field.NotEmpty");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "numbersMeetNumbers", ExceptionCodes.FILED_NOT_EMPTY, "field.NotEmpty");
 
      /*  
        if (user.getName().length() < 6 || user.getName().length() > 32) {
            errors.rejectValue("name", ExceptionCodes.FIELD_VALIDATION_ERROR, "userForm.username.Size");
        }
        try {
			if (userService.findByName(user.getName()) != null) {
			    errors.rejectValue("name", ExceptionCodes.FIELD_VALIDATION_ERROR, "userForm.username.Duplicate");
			}
		} catch (BusinessException | SystemException e) {
			e.printStackTrace();
		}

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "pwd", "NotEmpty");
        if (user.getPwd().length() < 8 || user.getPwd().length() > 32) {
            errors.rejectValue("pwd", ExceptionCodes.FIELD_VALIDATION_ERROR, "userForm.password.Size");
        }

        if (!user.getPwdConfirm().equals(user.getPwd())) {
            errors.rejectValue("pwdConfirm", ExceptionCodes.FIELD_VALIDATION_ERROR, "userForm.passwordConfirm.Diff");
        }
        
        */
    }
}
