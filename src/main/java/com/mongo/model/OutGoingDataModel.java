package com.mongo.model;

import java.util.Map;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Getter;
import lombok.Setter;

@Document("OutGoing")
@Getter@Setter
public class OutGoingDataModel {
    @Id
    private String id;
    private String refId;
    private Integer largestElement;
    private String whiteSpacesGalore;
    private Map<String,Integer> charCount;
    
}


