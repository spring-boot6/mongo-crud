package com.mongo.model;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Getter;
import lombok.Setter;

@Getter@Setter
@Document(collection = "IncomingData")
public class IncomingDataModel {
    @Id
    private String id;
    private String findDuplicates;
    private String whiteSpacesGalore;
    private Boolean validateMeOnlyIActuallyShouldBeABoolean;
    private List<Integer> numbersMeetNumbers;
}


