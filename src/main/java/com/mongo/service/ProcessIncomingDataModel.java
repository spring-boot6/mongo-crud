package com.mongo.service;

import java.util.List;
import java.util.Map;

public interface ProcessIncomingDataModel {
	Integer findLargest(List<Integer> numbers);
	Map<String,Integer> FindDuplicteCharactors(String data);
	String RemoveWhiteSpaces(String data);
}
