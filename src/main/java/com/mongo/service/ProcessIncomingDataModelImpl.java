package com.mongo.service;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

@Component
public class ProcessIncomingDataModelImpl implements ProcessIncomingDataModel {

	@Override
	public Integer findLargest(List<Integer> numbers) {
		return Collections.max(numbers);
	}

	@Override
	public Map<String,Integer> FindDuplicteCharactors(String data) {
		Map<String,Integer> charCount=new HashMap<String, Integer>(32);
		
		for (int i = 0;i < data.length(); i++){
			String str= Character.toString(data.charAt(i));
		    
		    if(charCount.containsKey(str)) {
		    	charCount.put(str, charCount.get(str)+1);
		    }else {
		    	charCount.put(str,1);
		    }
		}
		return charCount;
	}

	@Override
	public String RemoveWhiteSpaces(String data) {
		String[] array= data.split(" ");
		String newString="";
		for(String str:array){
		newString +=str;
		}
		return newString;
	}

}
