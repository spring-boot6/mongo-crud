package com.mongo.service;

import com.mongo.model.IncomingDataModel;

public interface IncomingDataModelService {
	IncomingDataModel save(IncomingDataModel incomingDataModel);
}
