package com.mongo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mongo.dao.IncomingDataModelRepository;
import com.mongo.dao.OutGoingDataModelRepository;
import com.mongo.model.IncomingDataModel;
import com.mongo.model.OutGoingDataModel;

import lombok.extern.log4j.Log4j2;

@Log4j2
@Component
public class IncomingDataModelServiceImpl implements IncomingDataModelService {
	@Autowired
	IncomingDataModelRepository incomingDataModelRepository;
	
	@Autowired
	OutGoingDataModelRepository outGoingDataModelRepository;
	
	@Autowired
	ProcessIncomingDataModel processIncomingDataModel;

	@Override
	public IncomingDataModel save(IncomingDataModel incomingDataModel) {
		IncomingDataModel reponse= incomingDataModelRepository.save(incomingDataModel);
		
		OutGoingDataModel outgoingDataModel=new OutGoingDataModel();
		outgoingDataModel.setRefId(reponse.getId());
		outgoingDataModel.setLargestElement(processIncomingDataModel.findLargest(reponse.getNumbersMeetNumbers()));
		outgoingDataModel.setCharCount(processIncomingDataModel.FindDuplicteCharactors(reponse.getFindDuplicates()));
		outgoingDataModel.setWhiteSpacesGalore(processIncomingDataModel.RemoveWhiteSpaces(reponse.getWhiteSpacesGalore()));
		OutGoingDataModel result =outGoingDataModelRepository.save(outgoingDataModel);
		log.debug("result:::"+result);
		return reponse;
	}

}
