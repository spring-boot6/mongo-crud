package com.mongo.dao;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.mongo.model.OutGoingDataModel;

@Repository
public interface OutGoingDataModelRepository extends MongoRepository<OutGoingDataModel, Integer> {
	OutGoingDataModel findById(String id);
}