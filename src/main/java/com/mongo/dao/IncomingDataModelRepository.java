package com.mongo.dao;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.mongo.model.IncomingDataModel;

@Repository
public interface IncomingDataModelRepository extends MongoRepository<IncomingDataModel, Integer> {
	IncomingDataModel findById(String id);
}