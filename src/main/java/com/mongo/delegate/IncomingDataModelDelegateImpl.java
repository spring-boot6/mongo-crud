package com.mongo.delegate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mongo.model.IncomingDataModel;
import com.mongo.service.IncomingDataModelService;

@Component
public class IncomingDataModelDelegateImpl implements IncomingDataModelDelegate {

	@Autowired
	IncomingDataModelService incomingDataModelService;
	
	@Override
	public IncomingDataModel save(IncomingDataModel incomingDataModel) {
		return incomingDataModelService.save(incomingDataModel);
	}

}
