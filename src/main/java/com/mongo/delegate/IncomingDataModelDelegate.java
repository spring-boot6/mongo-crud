package com.mongo.delegate;

import com.mongo.model.IncomingDataModel;

public interface IncomingDataModelDelegate {
	IncomingDataModel save(IncomingDataModel incomingDataModel);
}
