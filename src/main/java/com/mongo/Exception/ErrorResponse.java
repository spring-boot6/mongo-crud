package com.mongo.Exception;

import java.util.Map;

import lombok.Getter;
import lombok.Setter;

@Getter@Setter 
public class ErrorResponse
{
    private String errorCode;
    private String message;
    private Map<String, String> errorDetails;
 
}
