package com.mongo.Exception;

import org.springframework.validation.BindingResult;

public class ArgumentsNotValidException extends Exception {

	private static final long serialVersionUID = 5571274094812496215L;
	BindingResult bindingResult;

	public ArgumentsNotValidException(BindingResult bindingResult) {
		this.bindingResult = bindingResult;
	}

	public BindingResult getBindingResult() {
		return bindingResult;
	}

	public void setBindingResult(BindingResult bindingResult) {
		this.bindingResult = bindingResult;
	}

}
